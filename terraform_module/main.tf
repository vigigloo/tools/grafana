resource "random_password" "password" {
  count  = var.grafana_admin_password == null ? 1 : 0
  length = 128
}

locals {
  admin_password = var.grafana_admin_password == null ? random_password.password[0].result : var.grafana_admin_password
}

resource "kubernetes_secret" "admin" {
  count = var.grafana_admin_secret_name == null ? 0 : 1
  metadata {
    name      = var.grafana_admin_secret_name
    namespace = var.namespace
  }

  data = {
    admin-user     = var.grafana_admin_user
    admin-password = local.admin_password
  }
}

resource "helm_release" "grafana" {
  chart           = "grafana"
  repository      = "https://grafana.github.io/helm-charts"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = var.values

  dynamic "set" {
    for_each = var.grafana_admin_secret_name == null ? [] : [kubernetes_secret.admin[0].metadata[0].name]
    content {
      name  = "admin.existingSecret"
      value = set.value
    }
  }
}

data "kubernetes_ingress_v1" "grafana" {
  metadata {
    name      = helm_release.grafana.name
    namespace = helm_release.grafana.namespace
  }
}
