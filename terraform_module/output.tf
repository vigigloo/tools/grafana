output "grafana_admin_user" {
  sensitive = true
  value     = var.grafana_admin_user
}

output "grafana_admin_password" {
  sensitive = true
  value     = local.admin_password
}

locals {
  host     = try(data.kubernetes_ingress_v1.grafana.spec[0].rule[0].host, null)
  protocol = try(data.kubernetes_ingress_v1.grafana.spec[0].tls[0].hosts[0], null) == null ? "http" : "https"

}

output "host" {
  value = local.host
}

output "protocol" {
  value = local.protocol
}

output "url" {
  value = "${local.protocol}://${local.host}/"
}
